<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AWimport</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="flex h-screen bg-gray-500">
<div class="w-full max-w-xs m-auto bg-gray-100 rounded p-5">
    <!-- header -->
    <header>
        <img class="w-64 mx-auto mb-5" src="{{asset('assets/brand/awimport_logo.svg')}}" />
    </header>
    <!-- form -->
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div>
            <label class="block mb-2" for="email">Correo</label>
            <input class="w-full p-2 mb-6 border-b border-indigo-500 outline-none focus:bg-gray-300" autofocus type="text" name="email">
        </div>
        <div>
            <label class="block mb-2" for="password">Contraseña</label>
            <input class="w-full p-2 mb-6 border-b border-indigo-500 outline-none focus:bg-gray-300" type="password" name="password">
        </div>
        <div>
            <input class="w-full cursor-pointer bg-indigo-700 hover:bg-pink-700 text-white font-bold py-2 px-4 mb-6 rounded" type="submit">
        </div>
    </form>
</div>
</body>
</html>
