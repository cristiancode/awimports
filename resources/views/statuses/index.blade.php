@extends('dashboard.base')

@section('css')
    <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header"> Catalogo de Estatus
                    <div class="card-header-actions">
                        @if(auth()->user()->menuroles === 'admin')
                            <a href="{{ route('status.create') }}" class="btn btn-primary">Crear Estatus</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                    @endif
                    <div class="table-responsive">
                        <table class="display hover compact datatable" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($statuses as $status)
                                <tr>
                                    <td>{{$status->id}}</td>
                                    <td>{{$status->name}}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Vertical button group">
                                            @if(auth()->user()->menuroles === 'admin')
                                                <a href="{{ url('/master/status/' . $status->id . '/edit') }}" class="btn btn-sm btn-block btn-secondary">Editar</a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
@endsection
