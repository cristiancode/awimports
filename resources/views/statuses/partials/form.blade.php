<div class="card-body">
    <div class="form-group row">
        <div class="col">
            {{ Form::label('name', 'Status') }}
            {{ Form::text('name', null, ['class' => 'form-control','autofocus'=>'autofocus']) }}
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::submit('Guardar',['class' => 'btn btn-block btn-success']) }}
        </div>
        <div class="form-group col-sm-6">
            <a href="{{ route('status.index') }}" class="btn btn-block btn-primary">Cancelar</a>
        </div>
    </div>
</div>


