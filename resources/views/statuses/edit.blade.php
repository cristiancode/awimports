@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            Editar Estatus
                            <div class="card-header-actions">
                                <a href="{{ route('status.index') }}" class="btn btn-primary">Cancelar</a>
                            </div>
                        </div>
                        {!! Form::model($status, ['route' => ['status.update', $status->id],
                            'method' => 'PUT', 'files' => true]) !!}
                        @include('statuses.partials.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
