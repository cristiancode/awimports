@extends('dashboard.base')
@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header"><h4>Editar Contraseña de: {{ $user->name }}</h4></div>
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                            @endif
                            <form method="POST" action="{{ route('update.password', $user->id) }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}"/>
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <label for="password"><h4>Password</h4></label>
                                        <input type="password" class="form-control" name="password" placeholder="Nueva contraseña..." title="enter your password.">
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Guardar</button>
                                <a class="btn btn-primary" href="{{ route('users.index') }}">Atras</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
