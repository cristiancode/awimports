@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-6 col-md-5 col-lg-4 col-xl-4">
                <div class="card">
                    <div class="card-header">
                      <h4>{{ __('Usuario: ') }} {{ $user->name }}</h4>
                    </div>
                    <div class="card-body">
                        <h4>{{ __('Nombre: ') }}: {{ $user->name }}</h4>
                        <h4>{{ __('Correo: ') }}: {{ $user->email }}</h4>
                        <a href="{{ route('users.index') }}" class="btn btn-primary">{{ __('Regresar') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection
