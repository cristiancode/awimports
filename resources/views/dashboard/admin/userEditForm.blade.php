@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-6 col-md-5 col-lg-4 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <h4> {{ __('Editar: ') }} {{ $user->name }} </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="/users/{{ $user->id }}">
                            @csrf
                            @method('PUT')
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="cil-user"></i>
                                    </span>
                                </div>
                                <input class="form-control" type="text" placeholder="{{ __('Nombre de usuario') }}" name="name" value="{{ $user->name }}" required autofocus>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">@</span>
                                </div>
                                <input class="form-control" type="text" placeholder="{{ __('Correo') }}" name="email" value="{{ $user->email }}" required>
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>

                                <select class="form-control" id="role" name="role">
                                    @foreach($roles as $role)
                                        {{$role->name}}
                                        <option value="{{$role->name}}" {{$role->name === $user->menuroles ? 'selected': ''}}>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-success" type="submit">Guardar</button>
                            <a href="{{ route('users.index') }}" class="btn btn-primary">Atras</a>
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection

@section('javascript')

@endsection
