@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-6 col-md-5 col-lg-4 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4> Crear Usuario </h4>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form method="POST" action="/users">
                                @csrf
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="cil-user"></i>
                                    </span>
                                    </div>
                                    <input class="form-control" type="text" placeholder="Nombre de usuario" name="name" value="{{ old('name') }}" required autofocus>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@</span>
                                    </div>
                                    <input class="form-control" type="text" placeholder="{{ __('Correo') }}" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="role">Role</label>
                                    <select class="form-control" id="role" name="role">
                                        @foreach($roles as $role)
                                        <option value="{{$role->name}}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Password</span>
                                    </div>
                                    <input class="form-control" type="password" placeholder="{{ __('Password') }}" name="password" required>
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Confirmar Password</span>
                                    </div>
                                    <input class="form-control" type="password" placeholder="{{ __('Confirmar Password') }}" name="password_confirmation" required>
                                </div>
                                <button class="btn btn-success" type="submit">Guardar</button>
                                <a href="{{ route('users.index') }}" class="btn btn-primary">Atras</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
