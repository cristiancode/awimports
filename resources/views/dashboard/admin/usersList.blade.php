@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8">
                    <div class="card">
                        <div class="card-header">
                            <h4>{{ __('Usuarios') }}</h4>
                        </div>
                        <div class="card-body">
                            @if(Session::has('message'))
                                <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                            @endif
                            <table class="table table-responsive-sm table-striped">
                                <thead>
                                <tr>
                                    <th>{{ __('Nombre') }}</th>
                                    <th>{{ __('Correo') }}</th>
                                    <th>{{ __('Role') }}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->menuroles }}</td>
                                        <td>
                                            <a href="{{ url('/users/' . $user->id) }}" class="btn btn-block btn-primary">{{ __('Ver') }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ url('/users/' . $user->id . '/edit') }}" class="btn btn-block btn-primary">{{ __('Editar') }}</a>
                                        </td>
                                        <td>
                                            @if($user->menuroles !== 'admin')
                                            <a href="{{ route('edit.password', $user->id) }}" class="btn btn-block btn-primary">{{ __('Cambiar Contraseña') }}</a>
                                            @endif
                                        </td>
                                        <td>
                                            @if( $you->id !== $user->id )
                                                <form action="{{ route('users.destroy', $user->id ) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button class="btn btn-block btn-danger">{{ __('Eliminar') }}</button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('javascript')

@endsection

