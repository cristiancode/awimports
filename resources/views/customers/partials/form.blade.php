<div class="card-body">
    <div class="form-group row">
        <div class="col">
            {{ Form::label('first_name', 'Nombre') }}
            {{ Form::text('first_name', null, ['class' => 'form-control','autofocus'=>'autofocus', 'required' => true]) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('second_last_name', 'Apellido paterno') }}
            {{ Form::text('second_last_name', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('last_name', 'Apellido materno') }}
            {{ Form::text('last_name', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('phone', 'Teléfono') }}
            {{ Form::text('phone', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('email', 'Correo electrónico') }}
            {{ Form::text('email', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('ine', 'Número de Credencial de Elector') }}
            {{ Form::text('ine', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('rfc', 'RFC (Registro Federal de Contribuyentes)') }}
            {{ Form::text('rfc', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            {{ Form::label('address', 'Dirección') }}
            {{ Form::text('address', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-4">
            {{ Form::label('zipcode', 'Código Postal') }}
            {{ Form::text('zipcode', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-4">
            {{ Form::label('state', 'Estado') }}
            {{ Form::text('state', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-4">
            {{ Form::label('municipality', 'Ciudad/Provincia') }}
            {{ Form::text('municipality', null, ['class' => 'form-control']) }}
        </div>
    </div>
    {{--Begin input file--}}
    <div class="form-group row">
        <div class="col-md-4">
        {{ Form::label("avatar", "Foto de cliente", ['class' => 'col-md-4 col-form-label']) }}
            {{ Form::file("avatar") }}
        </div>
        <div class="col-md-6">
            @if(isset($customer->avatar))
                <figure class="figure">
                    <a href="{{asset($customer->avatar)}}" target="_blank">
                        <img src="{{asset($customer->avatar)}}" width="300" height="150" class="rounded mx-auto d-block" alt="">
                    </a>
                </figure>
            @endif
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::submit('Guardar',['class' => 'btn btn-block btn-success']) }}
        </div>
        <div class="form-group col-sm-6">
            <a href="{{ route('customers.index') }}" class="btn btn-block btn-primary">Cancelar</a>
        </div>
    </div>
</div>


