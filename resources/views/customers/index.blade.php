@extends('dashboard.base')

@section('css')
    <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="css/sweetalert2.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header"> Clientes
                    <div class="card-header-actions">
                        <a href="{{ route('customers.create') }}" class="btn btn-primary">Crear Cliente</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="display hover compact datatable" style="width:100%">
                        <thead>
                        <tr>
                            <th>Carnet</th>
                            <th>Nombre</th>
                            <th>Teléfono</th>
                            <th>Correo Electrónico</th>
                            <th>Fecha de Ingreso</th>
                            <th>Detalle</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($customers as $customer)
                            <tr class="{{$customer->is_active < 1 ? 'table-danger' : ''}}">
                                <td>
                                    <span class="badge badge-pill badge-success"> {{$customer->id}}</span>
                                   </td>
                                <td>{{$customer->fullName}}</td>
                                <td>{{$customer->phone}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->date}}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Vertical button group">
                                       {{-- <a href="{{ url('/customers/' . $customer->id) }}" class="btn btn-sm btn-secondary">Ver</a>--}}
                                        <a href="{{ url('/customers/' . $customer->id . '/edit') }}" class="btn btn-sm btn-block btn-secondary">Editar</a>
                                        <a href="#" class="btn btn-sm btn-secondary" type="button" onclick="deleteItem({{$customer->id}})">Eliminar</a>
                                    </div>
                                    <div class="p-1">
                                       {{-- <span class="{{ $customer->status->class }}">
                                        {{ $customer->status->nickname }}
                                        </span>--}}
                                        {{--@if($customer->status === 1)
                                            <span class="badge badge-pill badge-danger">
                                                Cargar Titulo
                                            </span>
                                        @endif--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /.modal-->
@endsection

@section('javascript')
    <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
    <script>
        function deleteItem(id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "Realmente quieres eliminar este cliente !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(`customers/${id}`).then( (response) => {
                        if(response.statusText === 'OK') {
                            Swal.fire({
                                icon: 'success',
                                confirmButtonText: 'Aceptar',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            });
                        }
                    })
                }
            })
        };
    </script>
@endsection
