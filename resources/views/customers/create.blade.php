@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            Crear Cliente
                            <div class="card-header-actions">
                                <a href="{{ route('customers.index') }}" class="btn btn-primary">Cancelar</a>
                            </div>
                        </div>
                        {!! Form::open(['url' => 'customers', 'files' => true, 'id' => 'customers']) !!}

                        @include('customers.partials.form')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    {{--<script src="{{ asset('js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/customers.js') }}"></script>--}}
@endsection

