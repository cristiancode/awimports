<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Recibo{{ $data->id }}</title>
    <style>
        *{
            margin: 0;
            padding: 0;
          /*  box-sizing: border-box;*/
        }
        p, label, span, table{
            font-size: 9pt;
        }

        #page_pdf{
            width: 95%;
            margin: 25px auto 20px auto;
        }

        #factura_head, #factura_detalle {
            width: 100%;
            margin-top: 15px;
            margin-bottom: 20px;
            font-size: 14px;
            margin-left: 20px;
        }
        .logo_factura{
            width: 100px;
        }
        .info_main_title {
            width: 90%;
            text-align: left;
            font-size: 15pt;
        }
        .info_empresa{
            width: 50%;
            text-align: right;
        }
        .datos_cliente tr td{
            width: 50%;
        }
        .datos_cliente label{
            width: 75px;
            display: inline-block;
        }
        .datos_cliente p{
            display: inline-block;
        }
        #factura_detalle{
            border-collapse: collapse;
        }
        .nota{
            font-size: 10pt;
            width: 100%;
            margin-left: 25px;
        }
        #factura_detalle th {
            text-align: left;
            width: 50%;
            padding: 10px;
            margin-right: 40px;
        }
        #firma_line {
            text-align: center;
            margin-left: 30px;
            width: 90%;
            margin-top: 80px;
        }
        #firma {
            width: 90%;
            margin-bottom: 50px;
            font-size: 14px;
            margin-left: 40px;
        }
        .terminos {
            margin-left: 30px;
        }
    </style>
</head>
<body>
<div id="page_pdf">
    <table>
        <tr>
            <td class="info_main_title">
                <p>{{ Carbon\Carbon::now()}}</p>
                <div style="padding:10px 0px">
                    RECIBO DE ENTRADA A IMPORTACION AWimport.<strong> #{{ $data->id }}</strong>
                </div>
            </td>
        </tr>
    </table>
    <hr>
    <table id="factura_head">
        <tr>
            <td class="logo_factura" colspan="2">
                <img src="{{public_path('assets/brand/awimport_logo.png')}}" width="250" height="70">
            </td>
            <td class="info_empresa">
                <p>Tel: 625-842-3201 / 915-275-3930</p>
                <p>Email: wem.imports@gmail.com</p>
            </td>
        </tr>
    </table>

    <table id="factura_detalle">
        <tbody>
        <tr>
            <th>Nombre del Cliente:</th>
            <td>{{$data->name}}</td>
        </tr>
        <tr>
            <th>No. de artículo</th>
            <td>{{$data->invoice}}</td>
        </tr>
        <tr>
            <th style="text-align:left;">No. de serie</th>
            <td>{{$data->serie}}</td>
        </tr>
        <tr>
            <th>Año</th>
            <td>{{ $data->year }}</td>
        </tr>
        <tr>
            <th>Tel</th>
            <td>{{ $data->phone }}</td>
        </tr>
        <tr>
            <th>Artículo</th>
            <td>{{ $data->item}}</td>
        </tr>
        <tr>
            <th>Origen</th>
            <td>{{$data->origin}}</td>
        </tr>
        <tr>
            <th>Destino</th>
            <td>{{ $data->destination }}</td>
        </tr>
        <tr>
            <th>Precio importación</th>
            <td>$ {{$data->import_price}}
                @if($data->coinType === 'Pesos')
                    {{$data->coinType}} MX
                    @else
                    {{$data->coinType}}
                @endif
            </td>
        </tr>
        <tr>
            <th>Documentos del Artículo</th>
            <td>{{$document->document_type}}</td>
        </tr>
        <tr>
            <th>Chofer que entrega</th>
            <td>_________________________________</td>
        </tr>
        </tbody>
    </table>
    <div>
        <p class="nota">Mediante la firma de este recibo me comprometo a cumplir los Términos y Condiciones de uso de AWimport.</p>
    </div>
    <table id="firma_line">
        <tr>
            <td>___________________________________</td>
            <td>_____________________________________</td>
        </tr>
    </table>
    <table id="firma">
        <tbody>
        <tr>
            <th>FIRMA DEL CLIENTE</th>
            <th>FIRMA DEL VENDEDOR</th>
        </tr>
        </tbody>
    </table>
    <div>
        <p class="terminos">1. Si el vehiculo ingresado excede los 20 días de estancia y no hay pago de importación, faltan papeles o cualquier otro,se le cobrará $5 USD diario a esa fecha.</p>
        <p class="terminos">2. A tres(3) meses de hacer caso omiso de renta, el vehículo quedará a disposición total de AWimport.</p>
        <p class="terminos">3. En caso de retirar su vehículo antes de los 20 días pactados y al no cumplir la importación en esta empresa, se cobrarán $100 USD por papeleo de vehículo.</p>
        <p class="terminos">4. El movimiento de persona en horas extra oficiales, se cobrará como cargo extra.</p>
        <p class="terminos">5. En caso de no mandar respuesta del recibo firmado, se dejará el recibo en el vehículo y valdrá como tal.</p>
        <p class="terminos">6. Mediante la firma de este recibo me comprometo a cumplir los términos y condiciones de AWimport.</p>
    </div>
</div>

</body>
</html>
