<div class="card-body">
    <div class="form-group row">
        <div class="col">
            {{ Form::label('name', 'Nombre') }}
            {{ Form::text('name', null, ['class' => 'form-control','autofocus'=>'autofocus']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('serie', 'N. Serie') }}
            {{ Form::text('serie', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('year', 'Año') }}
            {{ Form::text('year', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('phone', 'Teléfono') }}
            {{ Form::text('phone', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('item', 'Artículo') }}
            {{ Form::text('item', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('origin', 'Origen') }}
            {{ Form::text('origin', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('destination', 'Destino') }}
            {{ Form::text('destination', null, ['class' => 'form-control']) }}
        </div>
    </div>

    {{--Begin: Input Files--}}
    @foreach ($imagesTypes as $img)
        <div class="form-group row">
            {{ Form::label("{$img->name}", "{$img->type}", ['class' => 'col-md-2 col-form-label']) }}
            <div class="col-md-4">
                {{ Form::file("images[{$img->id}]") }}
            </div>
            <div class="col-md-4">
                @if(isset($receipt) && $receipt->img()->count())
                    @foreach($receipt->img as $pathImg)
                        @if($img->id === $pathImg->pivot->image_type_id)
                        <figure class="figure">
                            <a href="{{asset($pathImg->pivot->url)}}" target="_blank">
                                <img src="{{$pathImg->pivot->url}}" width="180" height="100" class="rounded mx-auto d-block" alt="">
                            </a>
                        </figure>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    @endforeach
    {{--Begin: End Files--}}

    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('emission', 'Fecha de Emisión') }}
            {{ Form::date('emission', \Carbon\Carbon::now(),['class' => 'form-control'] ) }}
        </div>
        <div class="form-group col-sm-6">
            {{ Form::label('Invoice', 'Folio') }}
            {{ Form::text('invoice', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('customer_price', 'Precio Cliente') }}
            {{ Form::text('customer_price', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group col-sm-6">
            <label class="col-md-3 col-form-label"></label>
            <div class="col-md-9 col-form-label">
                <div class="form-check">
                    {{ Form::radio('coinType', 'Dolares', false, ['class' => 'form-check-input']) }}
                    {{ Form::label('Dólares', null, ['class' => 'form-check-label']) }}
                </div>
                <div class="form-check">
                    {{ Form::radio('coinType', 'Pesos', false, ['class' => 'form-check-input']) }}
                    {{ Form::label('Pesos', null, ['class' => 'form-check-label']) }}

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::label('import_price', 'Precio de Importación') }}
            {{ Form::text('import_price', null, ['class' => 'form-control']) }}
        </div>
    </div>
    <div class="row">
        {{--Begin: Document_types --}}
        <div class="col-md-4 col-form-label">
            @foreach($documentsTypes as $type)
                <div class="form-check">
                    {{ Form::radio('has_documents', "{$type->id}", false, ['class' => 'form-check-input']) }}
                    {{ Form::label($type->document_type, null, ['class' => 'form-check-label']) }}
                </div>
            @endforeach
        </div>
        {{--End: Document_types --}}

        {{--Begin: has_documents --}}
        <div class="col-md-4 col-form-label">
            @foreach($documents as $doc)
            <div class="form-check">
                {{ Form::radio('document_type', "{$doc->id}", false, ['class' => 'form-check-input']) }}
                {{ Form::label($doc->document_name, null, ['class' => 'form-check-label']) }}
            </div>
            @endforeach
        </div>
        {{--Begin: has_documents --}}
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                {{ Form::label('Notas', null, ['class' => 'form-check-label']) }}
                {{ Form::textarea('notes',null, ['class' => 'form-control', 'rows' => '5', 'placeholder' => 'Agregar Notas...']) }}
            </div>
        </div>
    </div>
</div>
<div class="card-footer">
    <div class="row">
        <div class="form-group col-sm-6">
            {{ Form::submit('Guardar',['class' => 'btn btn-block btn-success']) }}
        </div>
        <div class="form-group col-sm-6">
            <a href="{{ route('receipts.index') }}" class="btn btn-block btn-primary">Cancelar</a>
        </div>
    </div>
</div>


