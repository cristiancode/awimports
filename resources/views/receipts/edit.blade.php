@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            Editar Recibo
                            <div class="card-header-actions">
                                <a href="{{ route('receipts.index') }}" class="btn btn-primary">Cancelar</a>
                            </div>
                        </div>
                        {!! Form::model($receipt, ['route' => ['receipts.update', $receipt->id],
                            'method' => 'PUT', 'files' => true]) !!}
                        @include('receipts.partials.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
