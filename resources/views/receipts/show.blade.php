@extends('dashboard.base')

@section('css')
    <link href="{{ asset('css/pdf/style.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">RECIBO DE ENTRADA A IMPORTACION AWimport. <strong> #{{ $receipt->id }}</strong>
                <a href="#" class="btn btn-sm btn-info mr-1 float-right" type="button" data-toggle="modal" data-target="#largeModal">Ver Imagenes</a>
                <a href="{{ url('/receipt/'.$receipt->id.'/print') }}" class="btn btn-sm btn-info float-right mr-1 d-print-none" target="_blank">Imprimir Recibo</a>
                <a class="btn btn-sm btn-info float-right mr-1 d-print-none" href="{{ route('receipts.index') }}">Atras</a>
            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-sm-8">
                        <img src="{{asset('assets/brand/awimport_logo.png')}}" width="250" height="70">
                    </div>
                    <!-- /.col-->
                    <div class="col-sm-4 text-right">
                        <div>{{\Carbon\Carbon::now()}}</div>
                        <div>Tel: 625-842-3201 / 915-275-3930</div>
                        <div>Email: wem.imports@gmail.com</div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
                <div class="table-responsive-sm">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th class="left">Nombre del Cliente</th>
                            <td class="left">{{ $receipt->name }}</td>
                        </tr>
                        <tr>
                            <th class="left">No. de artículo</th>
                            <td class="left"></td>
                        </tr>
                        <tr>
                            <th class="left">No. de serie</th>
                            <td class="left">{{ $receipt->serie }}</td>
                        </tr>
                        <tr>
                            <th class="left">Año</th>
                            <td class="left">{{$receipt->year}}</td>
                        </tr>
                        <tr>
                            <th class="left">Tel</th>
                            <td class="left">{{$receipt->phone}}</td>
                        </tr>
                        <tr>
                            <th class="left">Artículo</th>
                            <td class="left">{{$receipt->item}}</td>
                        </tr>
                        <tr>
                            <th class="left">Origen</th>
                            <td class="left">{{$receipt->origin}}</td>
                        </tr>
                        <tr>
                            <th class="left">Destino</th>
                            <td class="left">{{$receipt->destination}}</td>
                        </tr>
                        <tr>
                            <th class="left">Precio de importación</th>
                            <td class="left">{{$receipt->import_price}}</td>
                        </tr>
                        <tr>
                            <th class="left">Documentos del artículo</th>
                            <td class="left">{{$document->document_type}}</td>
                        </tr>
                        <tr>
                            <th class="left">Chofer que entrega</th>
                            <td class="left">__________________________</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12">Mediante la firma de este recibo me comprometo a cumplir los Términos y Condiciones de uso de AWimport.</div>
                </div>
                <div class="row">
                    <table id="firma_line">
                        <tr>
                            <td>___________________________________</td>
                            <td>_____________________________________</td>
                        </tr>
                    </table>
                    <table id="firma">
                        <tbody>
                        <tr>
                            <th style="text-align: center">FIRMA DEL CLIENTE</th>
                            <th style="text-align: center">FIRMA DEL VENDEDOR</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <p class="terminos">1. Si el vehiculo ingresado excede los 20 días de estancia y no hay pago de importación, faltan papeles o cualquier otro,se le cobrará $5 USD diario a esa fecha.</p>
                    <p class="terminos">2. A tres(3) meses de hacer caso omiso de renta, el vehículo quedará a disposición total de AWimport.</p>
                    <p class="terminos">3. En caso de retirar su vehículo antes de los 20 días pactados y al no cumplir la importación en esta empresa, se cobrarán $100 USD por papeleo de vehículo.</p>
                    <p class="terminos">4. El movimiento de persona en horas extra oficiales, se cobrará como cargo extra.</p>
                    <p class="terminos">5. En caso de no mandar respuesta del recibo firmado, se dejará el recibo en el vehículo y valdrá como tal.</p>
                    <p class="terminos">6. Mediante la firma de este recibo me comprometo a cumplir los términos y condiciones de AWimport.</p>
                </div>
            </div>
            <div class="card-footer">
                <div class="row justify-content-md-center">
                    <div class="col col-lg-2">
                    </div>
                     <div class="col-md-auto">
                         <a href="{{ url('/receipt/'.$receipt->id.'/print') }}" class="btn btn-info" target="_blank">Imprimir Recibo</a>
                        <a href="#" class="btn btn-info" type="button" data-toggle="modal" data-target="#largeModal">Ver Imagenes</a>
                    </div>
                     <div class="col col-lg-2">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-->
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="carousel slide" id="carouselExampleControls" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($receipt->img as $img)
                            <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                <img class="d-block w-100" data-src="holder.js/800x400?auto=yes&amp;bg=777&amp;fg=555&amp;text=First slide" alt="First slide [800x400]" src="{{$img->pivot->url}}" data-holder-rendered="true"></div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
    <!-- /.modal-->
@endsection
