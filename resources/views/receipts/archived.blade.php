@extends('dashboard.base')

@section('css')
    <link href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="css/sweetalert2.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header"> Recibos Archivados</div>
                <div class="card-body">
                    <table class="display hover compact datatable responsive" style="width:100%">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Teléfono</th>
                            <th>No.Serie</th>
                            <th>Articulo</th>
                            <th>Folio</th>
                            <th>Precio</th>
                            <th>Año</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($receipts as $receipt)
                            <tr class="{{$receipt->has_documents === 1 ? 'table-danger' : ''}}">
                                <td>{{$receipt->created_at}}</td>
                                <td>{{$receipt->name}}</td>
                                <td>{{$receipt->phone}}</td>
                                <td>{{$receipt->serie}}</td>
                                <td>{{$receipt->item}}</td>
                                <td>{{$receipt->invoice}}</td>
                                <td>{{$receipt->import_price}}</td>
                                <td>{{$receipt->year}}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Vertical button group">
                                        <a href="{{ url('/receipts/' . $receipt->id) }}" class="btn btn-sm btn-secondary">Ver</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-->
@endsection

@section('javascript')
    <script src="{{ asset('js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/sweetalert2.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
    <script>
        function deleteItem(id) {
            Swal.fire({
                title: 'Estas seguro?',
                text: "Realmente quieres eliminar este recibo !",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    axios.delete(`receipts/${id}`).then( (response) => {
                        console.log(this);
                        if(response.statusText === 'OK') {
                            Swal.fire({
                                icon: 'success',
                                confirmButtonText: 'Aceptar',
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            });
                        }
                    })
                }
            })
        };
    </script>
@endsection
