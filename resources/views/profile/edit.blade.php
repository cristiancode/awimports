@extends('dashboard.base')

@section('content')
<div class="container bootstrap snippet">
   <div class="row">
    <div class="col-sm-3"><!--left col-->


      <div class="text-center">
        {!! Form::model($user, ['route' => ['update.profile', $user->id], 'method' => 'PUT', 'files' => true]) !!}

        {!! Form::close() !!}
        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
        <h6>Agregar una foto diferente...</h6>
        <input type="file" name="avatar" class="text-center center-block file-upload">
      </div><br>

    </div><!--/col-3-->
        <div class="col-sm-9">
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                  <form class="form" action="#" method="post">
                      <div class="form-group">

                          <div class="col-xs-6">
                              <label for="first_name"><h4>Nombre</h4></label>
                              <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                          </div>
                      </div>

                      {{--<div class="form-group">
                          <div class="col-xs-6">
                              <label for="password"><h4>Password</h4></label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-xs-6">
                            <label for="password2"><h4>Verify</h4></label>
                              <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                          </div>
                      </div>--}}

                      <div class="form-group">
                           <div class="col-xs-12">
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Guardar</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
             </div><!--/tab-pane-->
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->

@endsection()
