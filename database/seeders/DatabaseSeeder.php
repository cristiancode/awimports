<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StatusSeeder::class,
            UsersSeeder::class,
            /*UsersAndNotesSeeder::class,*/
            ReceiptSeeder::class,
            CustomerSeeder::class,
            MenusTableSeeder::class,
            NotesSeeder::class,
            DocumentsSeeder::class,
            DocumentsTypeSeeder::class,
            ImageTypesSeeder::class,
        ]);
    }
}
