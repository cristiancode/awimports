<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ImageTypesSeeder extends Seeder
{
    public array $types = [
        ['name' => 'vehiculo', 'type' => 'Vehiculo 1'],
        ['name' => 'vehiculo', 'type' => 'Vehiculo 2'],
        ['name' => 'vehiculo', 'type' => 'Vehiculo 3'],
        ['name' => 'vehiculo', 'type' => 'Vehiculo 4'],
        ['name' => 'chasis', 'type' => 'Chasis'],
        ['name' => 'motor', 'type' => 'Motor'],
        ['name' => 'factura', 'type' => 'Fáctura'],
        ['name' => 'titulo', 'type' => 'Título'],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type) {
            \DB::table('image_types')->insert([
                'type' => $type['type'],
                'name' => $type['name'],
            ]);
        }
    }
}
