<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DocumentsTypeSeeder extends Seeder
{
    public array $types = [
        'Sín Documentos',
        'En Posesión De Cliente',
        'En Posesión De AWinport',
        'En Posesión De Action',
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type) {
            \DB::table('document_types')->insert([
                'document_type' => $type
            ]);
        }
    }
}
