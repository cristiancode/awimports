<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DocumentsSeeder extends Seeder
{
    public array $documents = [
        'Factura',
        'Factura Externa',
        'Título',
        'Titulo Externo',
        'Extensión De Título'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->documents as $document) {

            \DB::table('documents')->insert([
                'document_name' => $document
            ]);
        }
    }
}
