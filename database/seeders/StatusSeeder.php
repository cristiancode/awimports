<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    protected array $statuses = [
        [
            'name' => 'Borrador',
            'class' => 'badge badge-pill badge-info',
        ],
        [
            'name' => 'Parado',
            'class' => 'badge badge-pill badge-secondary',
        ],
        [
            'name' => 'Completado',
            'class' => 'badge badge-pill badge-success',
        ],
        [
            'name' => 'Cancelado',
            'class' => 'badge badge-pill badge-danger',
        ],
        [
            'name' => 'Expirado',
            'class' => 'badge badge-pill badge-warning',
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->statuses as $status) {
            DB::table('status')->insert([
                'name' => $status['name'],
                'class' => $status['class'],
            ]);
        }
    }
}
