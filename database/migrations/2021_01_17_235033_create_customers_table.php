<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nickname')->nullable();
            $table->string('avatar')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('second_last_name');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('ine')->nullable();
            $table->string('rfc')->nullable();
            $table->string('address')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('state')->nullable();
            $table->string('municipality')->nullable();
            $table->string('is_active')->default(1);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
