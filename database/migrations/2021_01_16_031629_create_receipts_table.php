<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name')->nullable();
			$table->string('serie')->nullable();
			$table->string('year')->nullable();
			$table->string('phone')->nullable();
			$table->string('item')->nullable();
			$table->string('origin')->nullable();
			$table->string('notes')->nullable();
			$table->string('destination')->nullable();
			$table->string('emission')->nullable();
			$table->string('customer_price')->nullable();
			$table->string('import_price')->nullable();
			$table->string('invoice')->nullable();
			$table->enum('coinType', ['Pesos', 'Dolares'])->nullable();
			$table->smallInteger('has_documents')->nullable();
			$table->smallInteger('document_type')->nullable();
			$table->integer('user_id')->unsigned()->index();
            $table->integer('status_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
