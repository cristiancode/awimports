<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nickname' => $this->faker->name,
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'second_last_name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'ine' => $this->faker->creditCardNumber,
            'rfc' => $this->faker->ean8,
            'address' => $this->faker->address,
            'zipcode' => $this->faker->postcode,
            'state' => $this->faker->state,
            'municipality' => $this->faker->state,
            'is_active' => rand(0,1),
            'user_id' => User::all()->random(1)->first()->id,
        ];
    }
}
