<?php

namespace Database\Factories;

use App\Models\Receipt;
use App\Models\Status;
use App\Models\User as User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReceiptFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Receipt::class;

    protected $coin_types = ['Pesos', 'Dolares'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'serie' => $this->faker->swiftBicNumber,
            'year' => $this->faker->year($max = 'now'),
            'phone' => $this->faker->phoneNumber,
            'item' => $this->faker->word,
            'origin' => $this->faker->country,
            'notes' => $this->faker->sentence,
            'destination' => $this->faker->country,
            'emission' => $this->faker->city,
            'customer_price' => $this->faker->randomNumber(2),
            'import_price' => $this->faker->randomNumber(2),
            'invoice' => $this->faker->word,
            'coinType' => $this->faker->numberBetween(1,2),
            'has_documents' => rand(1,4),
            'document_type' => rand(1,4),
            'user_id' => User::all()->random(1)->first()->id,
            'status_id' => Status::all()->random(1)->first()->id,
        ];
    }
}
