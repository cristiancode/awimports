<?php


namespace App\Services;


use Illuminate\Support\Facades\Storage;

class UploadImagesService
{
    public function uploadImages($images = [])
    {
        $paths_images = [];
        foreach ($images as $key => $image) {
            $path = Storage::disk('public')->put('images/cars', $image);
            array_push($paths_images, ['type' => $key, 'path' => $path]);
        }
        return $paths_images;
    }

    public function savePathImg($img_paths = [], $receipt_id)
    {
        foreach ($img_paths as $img_path) {
            \DB::table('image_types_receipts')->insert([
                'image_type_id' => $img_path['type'],
                'url' => asset($img_path['path']),
                'receipt_id' => $receipt_id
            ]);
        }
    }
}
