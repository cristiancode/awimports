<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index()
    {
        $statuses = Status::Orderby('id', 'ASC')->get();
        return view('statuses.index', compact('statuses'));
    }

    public function create()
    {
        return view('statuses.create');
    }

    public function store(Request $request)
    {
        $class = Status::get('class')->random();
        Status::create([
            'name'=> $request->input('name'),
            'class' => $class
        ]);
        $request->session()->flash('message', 'Estatus creado correctamente');
        return redirect()->route('status.index');
    }

    public function edit($id)
    {
        $status = Status::find($id);
        return view('statuses.edit', compact('status'));
    }

    public function update(Request $request, $id)
    {
         $status = Status::find($id);
         $status->name = $request->input('name');
         $status->save();

        $request->session()->flash('message', 'Estatus actualizado');
        return redirect()->route('status.index');
    }
}
