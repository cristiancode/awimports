<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\DocumentType;
use App\Models\DocumentTypesReceipt;
use App\Models\ImageTypeReceipt;
use App\Models\Receipt;
use App\Models\ImageType;
use App\Models\Status;
use App\Services\UploadImagesService;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ReceiptController extends Controller
{

    protected UploadImagesService $imagesService;

    public function __construct(UploadImagesService $imagesService)
    {
        $this->middleware('auth');
        $this->imagesService = $imagesService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Status::where('name', 'Archivado')->first()->id;
        $receipts = Receipt::orderByDesc('created_at')
            ->whereNotIn('status_id', [$status])
            ->get();

        return view('receipts.index', ['receipts' => $receipts]);
    }


    public function create()
    {
        $documents = Document::all();
        $documents_type = DocumentType::all();
        $image_types = ImageType::all();
        return view('receipts.create', [
            'documents' => $documents,
            'documentsTypes' => $documents_type,
            'imagesTypes' =>$image_types,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $receipt = Receipt::create([
            'name' => $request->name,
            'serie' => $request->serie,
            'year' => $request->year,
            'phone' => $request->phone,
            'item' => $request->item,
            'origin' => $request->origin,
            'notes' => $request->notes,
            'destination' => $request->destination,
            'emission' => $request->emission,
            'customer_price' => $request->customer_price,
            'import_price' => $request->import_price,
            'invoice' => $request->invoice,
            'coinType' => $request->coinType,
            'has_documents' => $request->has_documents,
            'document_type' => $request->document_type,
            'user_id' => Auth::user()->id,
            'status_id' => 1,
        ]);

        if($request->file('images')) {
           $paths = $this->imagesService->uploadImages($request->file('images'));
           $this->matchImagesReceipts($paths, $receipt);
        }

        $request->session()->flash('message', 'Recibo creado correctamente');
        return redirect()->route('receipts.index');
    }

    private function matchImagesReceipts($images = [], Receipt $receipt)
    {
        foreach ($images as $img) {

            $receipt->img()->attach($receipt->id,[
                'url' => asset($img['path']),
                'image_type_id' => $img['type'],
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function show(Receipt $receipt)
    {
        $data = $receipt->with('img')->where('id', $receipt->id)->first();
        $document_type = DocumentType::where('id', $data->has_documents)->first();
        return view('receipts.show', [
            'receipt' => $data,
            'document' => $document_type
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function edit(Receipt $receipt)
    {
        $documents = Document::all();
        $documents_type = DocumentType::all();
        $image_types = ImageType::all();
        return view('receipts.edit', [
            'receipt' => $receipt->with('img')->where('id', $receipt->id)->first(),
            'documents' => $documents,
            'documentsTypes' => $documents_type,
            'imagesTypes' => $image_types,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Receipt $receipt)
    {
        $receipt->fill($request->all());
        $receipt->save();

        if($request->file('images')) {
            $paths = $this->imagesService->uploadImages($request->file('images'));

            $imgdb = ImageTypeReceipt::where('receipt_id', $receipt->id)->get();

            foreach ($paths as $img) {
                ImageTypeReceipt::updateOrCreate(
                    ['image_type_id' => $img['type'], 'receipt_id' => $receipt->id],
                    ['url' => asset($img['path'])]
                );
            }
        }

        $request->session()->flash('message', 'Recibo creado correctamente');
        return redirect()->route('receipts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Receipt  $receipt
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $receipt = Receipt::find($id);

        if($receipt){
            $receipt->delete();
        }
        return response()->json($receipt, 200);
    }

    public function printReceipt($id)
    {

        $data = Receipt::where('id', $id)->first();
        $document_type = DocumentType::where('id', $data->has_documents)->first();
        $pdf = \PDF::loadView('receipts.receipt',[
            'data' => $data,
            'document' => $document_type
        ]);

        return $pdf->stream();
    }

    public function archivedList()
    {
        $status = Status::where('name', 'Archivado')->first('id');
        $receipts = Receipt::where('status_id', $status->id)->get();

        return view('receipts.archived', ['receipts' => $receipts]);
    }

    public function archived(Request $request, $id)
    {
        $receipt = Receipt::find($id);
        $receipt->status_id = Status::where('name', 'Archivado')->first('id')->id;
        $receipt->save();

        $request->session()->flash('message', "Recibo archivado");
        return redirect()->route('receipts.index');
    }
}
