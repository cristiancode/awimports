<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Services\UploadImagesService;
use App\Http\Requests\CustomerRequest;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller
{
    protected UploadImagesService $imagesService;

    public function __construct(UploadImagesService $imagesService)
    {
        $this->middleware('auth');
        $this->imagesService = $imagesService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderByDesc('created_at')->get();
        return view('customers.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $customer = Customer::create([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'second_last_name' => $request->input('second_last_name'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'ine' => $request->input('ine'),
            'rfc' => $request->input('rfc'),
            'address' => $request->input('address'),
            'zipcode' => $request->input('zipcode'),
            'state' => $request->input('state'),
            'municipality' => $request->input('municipality'),
            'user_id' => auth()->user()->id,
        ]);

        if($request->file('avatar')) {
            $path = Storage::disk('public')->put('images/avatars', $request->file('avatar'));
            $customer->fill(['avatar' => '/'.$path]);
            $customer->save();
        }

        return response()->json($customer, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customers.edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, Customer $customer)
    {
        $customer->fill($request->all());

        if($request->file('avatar')) {
            Storage::disk('public')->delete(asset($customer->avatar));
            $path = Storage::disk('public')->put('images/avatars', $request->file('avatar'));
            $customer->fill(['avatar' => '/'.$path]);
        }

        $customer->save();

        return redirect()->route('customers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        if($customer)
            $customer->delete();


        return response()->json($customer, 200);
    }
}
