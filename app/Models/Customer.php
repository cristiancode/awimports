<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Customer extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'nickname',
        'avatar',
        'first_name',
        'last_name',
        'second_last_name',
        'phone',
        'email',
        'ine',
        'rfc',
        'address',
        'zipcode',
        'state',
        'municipality',
        'is_active',
        'user_id',
    ];


    public function getDateAttribute($value): string
    {
        return $this->attributes['created_at'] = (new Carbon($value))->format('d-m-y');
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name} {$this->second_last_name}";
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = Str::title($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = Str::title($value);
    }

    public function setSecondLastNameAttribute($value)
    {
        $this->attributes['second_last_name'] = Str::title($value);
    }
}
