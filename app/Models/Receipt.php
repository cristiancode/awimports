<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'serie',
        'year',
        'phone',
        'item',
        'origin',
        'notes',
        'destination',
        'emission',
        'customer_price',
        'import_price',
        'invoice',
        'coinType',
        'user_id',
        'status_id',
        'has_documents',
        'document_type',
    ];

    /**
     * Get the Status that owns the Receipts.
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function img()
    {
        return $this->belongsToMany(Receipt::class, ImageTypeReceipt::class)
            ->withPivot('image_type_id','url', 'receipt_id');
    }
}
