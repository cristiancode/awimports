<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageTypeReceipt extends Model
{
    use HasFactory;

    protected $table = 'image_types_receipts';

    protected $fillable = ['url', 'image_type_id', 'receipt_id'];

    public $timestamps = false;
}
