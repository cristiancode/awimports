<?php

use App\Http\Controllers\ReceiptController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\StatusController;


Route::group(['middleware' => ['get.menu', 'check.auth']], function () {
    Route::get('/', function () { return view('auth.login'); });

    Auth::routes();

    Route::resource('receipts', 'ReceiptController');   //create receipts resource
    Route::get('receipt/{id}/print', [ReceiptController::class, 'printReceipt'])->name('receipt-print');
    Route::resource('customers',  'CustomerController');   //create customers resource
    Route::get('receipt/archived', [ReceiptController::class, 'archivedList'])->name('receipt.archived');
    Route::get('receipt/{id}/archived', [ReceiptController::class, 'archived'])->name('receipt.fileAway');

    Route::get('/edit/password/{id}', [UsersController::class, 'editPassword'])->name('edit.password');
    Route::post('/update/password/{id}', [UsersController::class, 'updatePassword'])->name('update.password');
    Route::resource('users','UsersController');

    Route::get('profile', [ProfileController::class, 'edit'])->name('edit.profile');
    Route::post('profile',[ProfileController::class, 'update'])->name('update.profile');

    Route::resource('roles','RolesController');

});

Route::group(['middleware' => ['check.auth']], function () {

    Route::prefix('master')->group(function () {
        Route::get('/users', function () {
            // Matches The "/admin/users" URL
        });

        Route::get('status', [StatusController::class, 'index'])->name('status.index');
        Route::get('status/create', [StatusController::class, 'create'])->name('status.create');
        Route::post('status', [StatusController::class, 'store'])->name('status.store');
        Route::get('status/{id}/edit', [StatusController::class, 'edit'])->name('status.edit');
        Route::put('status/{id}', [StatusController::class, 'update'])->name('status.update');

        Route::get('profile', [ProfileController::class, 'edit'])->name('edit.profile');
        Route::post('profile',[ProfileController::class, 'update'])->name('update.profile');

        Route::get('/roles/move/move-up',      'RolesController@moveUp')->name('roles.up');
        Route::get('/roles/move/move-down',    'RolesController@moveDown')->name('roles.down');

        Route::prefix('menu/element')->group(function () {
            Route::get('/',             'MenuElementController@index')->name('menu.index');
            Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
            Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
            Route::get('/create',       'MenuElementController@create')->name('menu.create');
            Route::post('/store',       'MenuElementController@store')->name('menu.store');
            Route::get('/get-parents',  'MenuElementController@getParents');
            Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
            Route::post('/update',      'MenuElementController@update')->name('menu.update');
            Route::get('/show',         'MenuElementController@show')->name('menu.show');
            Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
        });

        Route::get('/breadcrumb', function(){   return view('dashboard.base.breadcrumb'); });
        Route::get('/cards', function(){        return view('dashboard.base.cards'); });
        Route::get('/carousel', function(){     return view('dashboard.base.carousel'); });
        Route::get('/collapse', function(){     return view('dashboard.base.collapse'); });

        Route::get('/jumbotron', function(){    return view('dashboard.base.jumbotron'); });
        Route::get('/list-group', function(){   return view('dashboard.base.list-group'); });
        Route::get('/navs', function(){         return view('dashboard.base.navs'); });
        Route::get('/pagination', function(){   return view('dashboard.base.pagination'); });

        Route::get('/popovers', function(){     return view('dashboard.base.popovers'); });
        Route::get('/progress', function(){     return view('dashboard.base.progress'); });
        Route::get('/scrollspy', function(){    return view('dashboard.base.scrollspy'); });
        Route::get('/switches', function(){     return view('dashboard.base.switches'); });

        Route::get('/tabs', function () {       return view('dashboard.base.tabs'); });
        Route::get('/tooltips', function () {   return view('dashboard.base.tooltips'); });

        Route::get('/charts', function () {     return view('dashboard.charts'); });
        Route::get('/widgets', function () {    return view('dashboard.widgets'); });
        Route::get('/google-maps', function(){  return view('dashboard.googlemaps'); });
        Route::get('/404', function () {        return view('dashboard.404'); });
        Route::get('/500', function () {        return view('dashboard.500'); });
        Route::prefix('buttons')->group(function () {
            Route::get('/buttons', function(){          return view('dashboard.buttons.buttons'); });
            Route::get('/button-group', function(){     return view('dashboard.buttons.button-group'); });
            Route::get('/dropdowns', function(){        return view('dashboard.buttons.dropdowns'); });
            Route::get('/brand-buttons', function(){    return view('dashboard.buttons.brand-buttons'); });
            Route::get('/loading-buttons', function(){  return view('dashboard.buttons.loading-buttons'); });
        });
        Route::prefix('editors')->group(function () {
            Route::get('/code-editor', function(){          return view('dashboard.editors.code-editor'); });
            Route::get('/markdown-editor', function(){      return view('dashboard.editors.markdown-editor'); });
            Route::get('/text-editor', function(){          return view('dashboard.editors.text-editor'); });
        });

        Route::prefix('icon')->group(function () {  // word: "icons" - not working as part of adress
            Route::get('/coreui-icons', function(){         return view('dashboard.icons.coreui-icons'); });
            Route::get('/flags', function(){                return view('dashboard.icons.flags'); });
            Route::get('/brands', function(){               return view('dashboard.icons.brands'); });
        });

        Route::prefix('notifications')->group(function () {
            Route::get('/alerts', function(){               return view('dashboard.notifications.alerts'); });
            Route::get('/badge', function(){                return view('dashboard.notifications.badge'); });
            Route::get('/modals', function(){               return view('dashboard.notifications.modals'); });
            Route::get('/toastr', function(){               return view('dashboard.notifications.toastr'); });
        });
        Route::prefix('plugins')->group(function () {
            Route::get('/calendar', function(){             return view('dashboard.plugins.calendar'); });
            Route::get('/draggable-cards', function(){      return view('dashboard.plugins.draggable-cards'); });
            Route::get('/spinners', function(){             return view('dashboard.plugins.spinners'); });
        });

        Route::prefix('tables')->group(function () {
            Route::get('/', function () {             return view('dashboard.tables.tables'); });
            Route::get('/datatables', function () {         return view('dashboard.tables.datatables'); });
        });

        Route::prefix('apps')->group(function () {
            Route::prefix('invoicing')->group(function () {
                Route::get('/invoice', function () {        return view('dashboard.apps.invoicing.invoice'); });
            });
        });

        Route::prefix('email')->group(function () {
            Route::get('/inbox', function () {          return view('dashboard.apps.email.inbox'); });
            Route::get('/message', function () {        return view('dashboard.apps.email.message'); });
            Route::get('/compose', function () {        return view('dashboard.apps.email.compose'); });
        });

        Route::resource('notes', 'NotesController');
    });

});

/*Route::get('locale', 'LocaleController@locale');*/
